import MyArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by kirillk on 25.07.17.
 */
public class MyArrayListSuite {
    @Test
    void sizeTestCase(){
        MyArrayList list = new MyArrayList();
        Assert.assertTrue(list.size() == 0, true, "initial array size" );
    }

    @Test
    void addTestCase(){
        MyArrayList list = new MyArrayList();
        list.add(3);
        list.add(5);
        list.add(4);
        list.add(5);
        Assert.assertTrue(list.size() == 4, true, "four Integer objects added");
    }

    @Test
    void clearTestCase(){
        MyArrayList list = new MyArrayList();
        Assert.assertTrue(list.size() == 0, true, "array size after clear" );
    }

    @Test
    void isEmptyTestCase(){
        MyArrayList list = new MyArrayList();
        Assert.assertTrue(list.size() == 0, true, "array is Empty" );
    }
}
